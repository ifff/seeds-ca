with (import <nixpkgs> {});
stdenv.mkDerivation rec {
   name = "seeds-ca";
   buildInputs = [
     gcc
     gnumake
     SDL2
   ];
}
